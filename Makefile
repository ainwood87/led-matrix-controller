CC = gcc

#COMPILER_FLAGS = -w

LINKER_FLAGS = -lSDL2 -lSDL2_image

OBJ_NAME = sdl_input

INCLUDE_PATH = $(shell pwd)/res

OBJS = sdl_input.c

all: $(OBJS)
	$(CC) $(OBJS) -I$(INCLUDE_PATH) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)

clean:
	rm $(OBJ_NAME)
