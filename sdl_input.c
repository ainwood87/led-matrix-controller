#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#ifdef _WIN32
#define PATH_SEP "\\"
#else
#define PATH_SEP "/"
#endif

#define PNG_WIDTH (50)
#define NUM_LED (8)
#define PADDING (PNG_WIDTH / 6)

#define W_WIDTH  (PNG_WIDTH + (PNG_WIDTH + PADDING) * (NUM_LED - 1))
#define W_HEIGHT W_WIDTH

static const char * resource_rel_path = "res";
static char * base_path;

typedef struct {
    bool enabled;
} led_state_t;

led_state_t led_grid[NUM_LED][NUM_LED];

SDL_Surface * red_circle_surface;
SDL_Surface * white_circle_surface;

static const char * get_resource_path() {
    static char * resource_path = NULL;
    if (resource_path == NULL) { //build it
        base_path = SDL_GetBasePath();
        int path_len = strlen(base_path) + strlen(resource_rel_path);
        resource_path = calloc(path_len + 1, sizeof(char));
        strcpy(resource_path, base_path);
        strcat(resource_path, resource_rel_path);
        strcat(resource_path, PATH_SEP);
    }
    return resource_path;
}

static SDL_Surface * load_surface (SDL_Surface * screen, const char * path)
{
    SDL_Surface * optimized_surface = NULL;
    SDL_Surface * loaded_surface = IMG_Load(path);
    if (NULL != loaded_surface) {
        optimized_surface = SDL_ConvertSurface(loaded_surface, screen->format, 0);
        SDL_FreeSurface(loaded_surface);
    }

    return optimized_surface;
}

static void update_led_state(SDL_Surface * screen)
{
    SDL_Rect dest = {
        .x = 0,
        .y = 0,
    };
    SDL_Surface * led_surface;
    led_state_t * led_state;
    int row, col;
    for (col = 0; col < NUM_LED; ++col) {
        for (row = 0; row < NUM_LED; ++row) {
            led_state = &led_grid[row][col];
            led_surface = led_state->enabled ? red_circle_surface : white_circle_surface;
            SDL_BlitSurface(led_surface, NULL, screen, &dest);
            dest.x += PNG_WIDTH + PADDING;
        }
        dest.y += PNG_WIDTH + PADDING ;
        dest.x = 0;
    }
}

int main(int argc, char * argv[])
{
    int rc = EXIT_SUCCESS;
    Uint32 init_flags = SDL_INIT_VIDEO | SDL_INIT_EVENTS;
    if (0 != (rc = SDL_Init(init_flags))) {
        fprintf(stderr, "\nUnable to init SDL: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    //create window to capture input events
    Uint32 window_flags = 0;
    SDL_Window * window = SDL_CreateWindow("led grid", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, W_WIDTH, W_HEIGHT, window_flags);
    if (NULL == window) {
        fprintf(stderr, "\nFailed to create window\n");
        rc = EXIT_FAILURE;
        goto fail0;
    }

    int img_flags = IMG_INIT_PNG;
    if (0 == IMG_Init(img_flags) & img_flags) {
        fprintf(stderr, "failed to init SDL_image\n");
        rc = EXIT_FAILURE;
        goto fail1;
    }
    
    SDL_Surface * screen = SDL_GetWindowSurface(window);
    if (NULL == screen) {
        fprintf(stderr, "\nFailed to get screen\n");
        rc = EXIT_FAILURE;
        goto fail2;
    }

    //load the surfaces
    char red_circle_path[100];
    strcpy(red_circle_path, get_resource_path());
    strcat(red_circle_path, "red_circle.png");

    red_circle_surface = load_surface(screen, red_circle_path);
    if (NULL == red_circle_surface) {
        fprintf(stderr, "\nFailed to load red circle\n");
        rc = EXIT_FAILURE;
        goto fail2;
    }

    char white_circle_path[100];
    strcpy(white_circle_path, get_resource_path());
    strcat(white_circle_path, "white_circle.png");

    white_circle_surface = load_surface(screen, white_circle_path);
    if (NULL == white_circle_surface) {
        fprintf(stderr, "\nFailed to load white circle\n");
        rc = EXIT_FAILURE;
        goto fail2;
    }

    //draw initial state
    update_led_state(screen);
    SDL_UpdateWindowSurface(window);

    //main loop
    for (;;) {
        static SDL_Event event;
        while (SDL_PollEvent(&event)) {
            int row, col;
            switch (event.type) {
                case SDL_MOUSEBUTTONDOWN:
                {
                    row = event.button.x / (PNG_WIDTH + PADDING);
                    col = event.button.y / (PNG_WIDTH + PADDING);
                    led_grid[row][col].enabled = !led_grid[row][col].enabled;
                    update_led_state(screen);
                    SDL_UpdateWindowSurface(window);
                }
                    break;
                case SDL_QUIT:
                    goto quit;
            }
        }
        SDL_Delay(100);
    }

quit:
fail2:
    IMG_Quit();
fail1:
    SDL_DestroyWindow(window);
fail0:
    //cleanup
    free (base_path);
    SDL_Quit();
    return rc;;
}
